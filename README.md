# Use Instagram API to get top 10 Images

## Accessing Project
Open Terminal / Command Prompt

Locate the folder where you want the project files to be downloaded

`git clone https://gitlab.com/charlpcronje/insta.git .`

The project has got some dependencies, composer needs to be on your system:
> `composer install`

## Project Config

cd path/to/instagram/dir

The .env file comntains the project config:

Linux: `cp .env.example .env`
Windows: `copy .env.example .env`

pen the .env file and provide values for the following:

INSTAGRAM_CALBACK_URL=http://localhost:8000/instagram/callback

INSTAGRAM_CLIENT_ID={Instagram Client ID}

INSTAGRAM_CLIENT_SECRET={Instagram Client Secret}

> Also remove the '{' and '}' when replacing your values

## Testing the project on your local machine

Laravel has a built in php server, to access this server type the following command:

`php artisan serve`

Open your browser (No internet explorer is not a real browser is only used to download real browsers)

Enter the following URL: `http://localhost:8000`

You will be redirected to Instagram for authentication.