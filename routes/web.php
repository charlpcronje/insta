<?php

Route::get('/', 'InstagramController@redirectToInstagram');
Route::get('/dashboard', 'InstagramController@dashboard');
Route::get('/instagram/callback', 'InstagramController@callback');